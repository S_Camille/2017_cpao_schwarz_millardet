package comparator;

import java.util.Comparator;
import classe.Etudiant;
import exception.FormaException2;

/**
 * Classe GroupeMeriteComparator qui implemente l'interface Comparator<Etudiant>
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class GroupeMeriteComprator implements Comparator<Etudiant> {

	@Override
	/**
	 * Methode compare qui retourne -1 si la moyenne de l'etudiant 2 est en-dessous de celle de l'etudiant 1, 0 si l'etudiant 2 a la meme moyenne que celle de l'etudiant 1, 1 si la moyenne de l'etudiant 2 est au-dessus de celle de l'etudiant 1
	 * 
	 * @return -1 si la moyenne de l'etudiant 2 est en-dessous de celle de l'etudiant 1, 0 si l'etudiant 2 a la meme moyenne que celle de l'etudiant 1, 1 si la moyenne de l'etudiant 2 est au-dessus de celle de l'etudiant 1
	 */
	public int compare(Etudiant etu1, Etudiant etu2){
		try{
			Double note1 = etu1.calcMoyGenerale();
			Double note2 = etu2.calcMoyGenerale();
			if (note1 > note2){
				return -1;
			}
			if (note1 < note2){
				return 1; 
			}
			return 0;
		}
		catch(FormaException2 e){
			return 2;
		}
	}
	
}