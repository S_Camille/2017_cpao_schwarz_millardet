package comparator;

import java.util.Comparator;
import classe.Etudiant;

/**
 * Classe GroupeAlphaComparator qui implemente l'interface Comparator<Etudiant>
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class GroupeAlphaComparator implements Comparator<Etudiant> {

	@Override
	/**
	 * Methode compare qui retourne -1 si l'etudiant 2 est avant l'etudiant 1, 0 si l'etudiant 2 est a la meme place que l'etudiant 1, 1 si l'etudiant 2 est apres l'etudiant 1
	 * 
	 * @return -1 si l'etudiant2 est avant l'etudiant 1, 0 si l'etudiant 2 est a la meme place que l'etudiant 1, 1 si l'etudiant 2 est apres l'etudiant 1
	 */
	public int compare(Etudiant etu1, Etudiant etu2) {
		return etu1.getNomEtu().compareTo(etu2.getNomEtu());
	}

}