package test;

import static org.junit.Assert.*;
import java.util.*;
import org.junit.*;
import classe.*;
import exception.FormaException2;

/**
 * Classe de tests de la classe Groupe
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class GroupeTest {

	/**
	 * Atrributs ident1, ident2, ident3, ident4 : Identites des etudiants du groupe
	 */
	private Identite ident1;
	private Identite ident2;
	private Identite ident3;
	private Identite ident4;
	
	/**
	 * Formation que suivent les etudiants du groupe
	 */
	private Formation forma;
	
	/**
	 * Attributs etu1, etu2, etu3, etu4 : Etudiants du groupe
	 */
	private Etudiant etu1;
	private Etudiant etu2;
	private Etudiant etu3;
	private Etudiant etu4;
	
	/**
	 * Groupe d'etudiants
	 */
	private Groupe gr;
	
	/**
	 * Methode creerGroupe qui permet d'initialiser les tests en creant un groupe d'etudiants
	 */
	@Before
	public void creerGroupe() {
		this.ident1 = new Identite("123456","Diebold","Aymerik");
		this.ident2 = new Identite("789012","Schwarz","Camille");
		this.ident3 = new Identite("904567","Millardet","Quentin");
		this.ident4 = new Identite("268132","Capone","Florian");
		Map<String, Integer> mat = new HashMap<String, Integer>();
		mat.put("Maths", 8);
		mat.put("Informatique", 12);
		mat.put("Anglais", 6);
		mat.put("Conception et Programmation Objet Avancees", 10);
		mat.put("Services Reseau", 4);
		this.forma = new Formation(789, mat);
		this.etu1 = new Etudiant(this.ident1, this.forma);
		this.etu1.ajoutNote("Services Reseau", 6);
		this.etu1.ajoutNote("Anglais", 7);
		this.etu1.ajoutNote("Anglais", 2);
		this.etu1.ajoutNote("Services Reseau", 14);
		this.etu1.ajoutNote("Services Reseau", 13);
		this.etu2 = new Etudiant(this.ident2, this.forma);
		this.etu2.ajoutNote("Services Reseau", 19);
		this.etu2.ajoutNote("Anglais", 13);
		this.etu2.ajoutNote("Anglais", 2);
		this.etu2.ajoutNote("Services Reseau", 10);
		this.etu2.ajoutNote("Services Reseau", 13);
		this.etu3 = new Etudiant(this.ident3, this.forma);
		this.etu3.ajoutNote("Anglais", 1);
		this.etu4 = new Etudiant(this.ident4, this.forma);
		this.etu4.ajoutNote("Services Reseau", 1);
		this.gr = new Groupe(this.forma);
	}
	
	/**
	 * Methode testAjoutEtu1 qui permet de tester l'ajout d'un etudiant a un groupe
	 */
	@Test
	public void testAjoutEtu1() {
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		List<Etudiant> collEtu = this.gr.getCollEtudiants();
		//assertion
		assertEquals("Le nom de l'eleve ajoute devrait etre 'Diebold'.", "Diebold", collEtu.get(0).getNomEtu());
	}

	/**
	 * Methode testAjoutEtu2 qui permet de tester l'ajout de deux etudiants a un groupe
	 */	
	@Test
	public void testAjoutEtu2() {
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		List<Etudiant> collEtu = this.gr.getCollEtudiants();
		//assertions
		assertEquals("Le nom de l'eleve ajoute devrait etre 'Diebold'.", "Diebold", collEtu.get(0).getNomEtu());
		assertEquals("Le nom de l'eleve ajoutee devrait etre 'Schwarz'.", "Schwarz", collEtu.get(1).getNomEtu());
	}
	
	/**
	 * Methode testAjoutEtu3 qui permet de tester l'ajout de deux etudiants identiques
	 */
	@Test
	public void testAjoutEtu3() {
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		this.gr.ajoutEtu(this.etu1);
		List<Etudiant> collEtu = this.gr.getCollEtudiants();
		//assertions
		assertEquals("Le nom de l'eleve ajoute devrait etre 'Diebold'.", "Diebold", collEtu.get(0).getNomEtu());
		assertEquals("Le nom de l'eleve ajoutee devrait etre 'Schwarz'.", "Schwarz", collEtu.get(1).getNomEtu());
		assertEquals("La liste devrait contenir uniquement deux etudiants", 2, collEtu.size());
	}
	
	/**
	 * Methode testAjoutEtu4 qui permet de tester l'ajout de deux etudiants identiques
	 */
	@Test
	public void testAjoutEtu4() {
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		this.gr.ajoutEtu(this.etu1);
		Identite ident5 = new Identite("432543", "Morival", "Fabien");
		Map<String, Integer> mat = new HashMap<String, Integer>();
		mat.put("Anglais", 1);
		Formation forma2 = new Formation(859475,mat);
		Etudiant etu5 = new Etudiant(ident5,forma2);
		this.gr.ajoutEtu(etu5);
		List<Etudiant> collEtu = this.gr.getCollEtudiants();
		//assertions
		assertEquals("Le nom de l'eleve ajoute devrait etre 'Diebold'.", "Diebold", collEtu.get(0).getNomEtu());
		assertEquals("Le nom de l'eleve ajoutee devrait etre 'Schwarz'.", "Schwarz", collEtu.get(1).getNomEtu());
		assertEquals("La liste devrait contenir uniquement deux etudiants", 2, collEtu.size());
	}
	
	/**
	 * Methode testSupprEtu1 qui permet de tester la suppression d'un etudiant
	 */
	@Test
	public void testSupprEtu1(){
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		this.gr.supprEtu(this.etu1);
		List<Etudiant> collEtu = this.gr.getCollEtudiants();
		//assertion
		assertEquals("Le nom de l'eleve ajoutee devrait etre 'Schwarz'.", "Schwarz", collEtu.get(0).getNomEtu());
	}
	
	/**
	 * Methode testSupprEtu2 qui permet de tester la suppression d'un etudiant qui n'est pas dans le groupe
	 */
	@Test
	public void testSupprEtu2(){
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		this.gr.supprEtu(this.etu3);
		List<Etudiant> collEtu = this.gr.getCollEtudiants();
		//assertions
		assertEquals("Le nom de l'eleve ajoute devrait etre 'Diebold'.", "Diebold", collEtu.get(0).getNomEtu());
		assertEquals("Le nom de l'eleve ajoutee devrait etre 'Schwarz'.", "Schwarz", collEtu.get(1).getNomEtu());
		assertEquals("Il devrait rester deux etudiants dans la liste.", 2, collEtu.size());
	}
	
	/**
	 * Methode testCalcMoyMatGr1 qui permet de tester le calcul de la moyenne d'un groupe d'etudiants pour une matiere
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalcMoyMatGr1() throws FormaException2{
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		//assertion
		assertEquals("La moyenne de la matiere 'Services Reseau' du groupe devrait etre de 12,50.", (Double)12.50, (Double)this.gr.calcMoyMatGr("Services Reseau"));
	}
	
	/**
	 * Methode testCalcMoyMatGr2 qui permet de tester le calcul de la moyenne d'un groupe d'etudiants pour une matiere qui n'existe pas
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalcMoyMatGr2() throws FormaException2{
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		//assertion
		assertEquals("La moyenne de Japonais ne devrait pas exister.", (Double)00.00, (Double)this.gr.calcMoyMatGr("Japonais"));
	}

	/**
	 * Methode testCalcMoyMatGr3 qui permet de tester le calcul de la moyenne d'un groupe d'etudiants pour une matiere, avec un etudiant qui n'a pas de notes
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalMoyMatGr3() throws FormaException2{
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		Identite ident5 = new Identite("432543", "Morival", "Fabien");
		Etudiant etu5 = new Etudiant(ident5, this.forma);
		this.gr.ajoutEtu(etu5);
		//assertion
		assertEquals("La moyenne de la matiere 'Services Reseau' du groupe devrait etre de 12,50.", (Double)12.50, (Double)this.gr.calcMoyMatGr("Services Reseau"));
	}
	
	/**
	 * Methode testCalcMoyMatGr4 qui permet de tester le calcul de la moyenne d'un groupe d'etudiants pour une matiere, avec que des etudiants qui n'ont pas de notes
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalMoyMatGr4() throws FormaException2{
		//instructions de test
		Identite ident5 = new Identite("432543", "Morival", "Fabien");
		Etudiant etu5 = new Etudiant(ident5, this.forma);
		this.gr.ajoutEtu(etu5);
		Identite ident6 = new Identite("534904", "Salou", "Maxime");
		Etudiant etu6 = new Etudiant(ident6, this.forma);
		this.gr.ajoutEtu(etu6);
		Identite ident7 = new Identite("432453", "Vomscheid", "Gwendolyn");
		Etudiant etu7 = new Etudiant(ident7, this.forma);
		this.gr.ajoutEtu(etu7);
		//assertion
		assertEquals("La moyenne de la matiere 'Services Reseau' du groupe devrait etre de 0,00.", (Double)0.00, (Double)this.gr.calcMoyMatGr("Services Reseau"));
	}
	
	/**
	 * Methode testCalcMoyGeneraleGr1 qui permet de tester le calcul de la moyenne generale d'un groupe d'etudiants
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalcMoyGeneraleGr1() throws FormaException2{
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		//assertion
		assertEquals("La moyenne generale du groupe devrait etre de 8,60.", (Double)8.60, (Double)this.gr.calcMoyGeneraleGr());
	}
	
	/**
	 * Methode testCalcMoyGeneraleGr2 qui permet de tester le calcul de la moyenne generale d'un groupe d'etudiants, qui ne contient pas d'etudiants
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalcMoyGeneraleGr2() throws FormaException2{
		//assertion
		assertEquals("La moyenne generale du groupe devrait etre de 0,00.", (Double)0.00, (Double)this.gr.calcMoyGeneraleGr());
	}
	
	/**
	 * Methode testTriParMerite1 qui permet de tester le tri par merite pour un groupe d'etudiants
	 */
	@Test
	public void testTriParMerite1(){
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		this.gr.ajoutEtu(this.etu3);
		this.gr.ajoutEtu(this.etu4);
		this.gr.triParMerite();
		List<Etudiant> g = this.gr.getCollEtudiants();
		//assertions
		assertEquals("La premiere eleve devrait etre Schwarz Camille.", "Schwarz", g.get(0).getNomEtu());
		assertEquals("Le deuxieme eleve devrait etre Diebold Aymerik.", "Diebold", g.get(1).getNomEtu());
		assertEquals("Le troisieme eleve devrait etre Millardet Quenin.", "Millardet", g.get(2).getNomEtu());
		assertEquals("Le quatrieme eleve devrait etre Capone Florian.", "Capone", g.get(3).getNomEtu());
	}
	
	/**
	 * Methode testTriParMerite2 qui permet de tester le tri par merite pour un groupe d'etudiants, qui n'est compose d'aucun etudiant
	 */
	@Test
	public void testTriParMerite2(){
		//instructions de test
		this.gr.triParMerite();
		List<Etudiant> g = this.gr.getCollEtudiants();
		//assertion
		assertEquals("La liste devrait etre vide.", 0, g.size());
	}
	
	/**
	 * Methode testTriAlpha qui permet de tester le tri par ordre alphabetique pour un groupe d'etudiants
	 */
	@Test
	public void testTriAlpha(){
		//instructions de test
		this.gr.ajoutEtu(this.etu1);
		this.gr.ajoutEtu(this.etu2);
		this.gr.ajoutEtu(this.etu3);
		this.gr.ajoutEtu(this.etu4);
		this.gr.triAlpha();
		List<Etudiant> g = this.gr.getCollEtudiants();
		//assertions
		assertEquals("Le premier eleve devrait etre Capone Florian.", "Capone", g.get(0).getNomEtu());
		assertEquals("Le deuxieme eleve devrait etre Diebold Aymerik.", "Diebold", g.get(1).getNomEtu());
		assertEquals("Le troisieme eleve devrait etre Millardet Quentin.", "Millardet", g.get(2).getNomEtu());
		assertEquals("La quatrieme eleve devrait etre Schwarz Camille.", "Schwarz", g.get(3).getNomEtu());
	}
	
	/**
	 * Methode testTriAlpha2 qui permet de tester le tri par ordre alphabetique pour un groupe d'etudiants, qui n'est compose d'aucun etudiant
	 */
	@Test
	public void testTriAlpha2(){
		//instructions de test
		this.gr.triAlpha();
		List<Etudiant> g = this.gr.getCollEtudiants();
		//assertion
		assertEquals("La liste devrait etre vide.", 0, g.size());
	}
	
}