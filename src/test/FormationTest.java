package test;

import static org.junit.Assert.*;
import java.util.*;
import org.junit.*;
import classe.Formation;
import exception.*;

/**
 * Classe de tests de la classe Formation
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class FormationTest {

	/**
	 * Identifiant de la formation
	 */
	private int id;
	/**
	 * Formation
	 */
	private Formation forma;
	
	/**
	 * Methode creerForma qui permet d'initialiser les tests en creant une formation
	 */
	@Before
	public void creerForma() {
		this.id = 999;
		Map<String,Integer> collMat = new HashMap<String,Integer>();
		collMat.put("Maths", 8);
		collMat.put("Informatique", 12);
		collMat.put("Anglais", 6);
		collMat.put("Conception et Programmation Objet Avancees", 10);
		collMat.put("Services Reseau", 4);
		this.forma = new Formation(this.id, collMat);
	}
	
	/**
	 * Methode testAjoutMatiere1 qui permet de tester l'ajout d'une matiere dans une formation
	 * 
	 * @throws FormaException
	 */
	@Test
	public void testAjoutMatiere1() throws FormaException {
		//instructions de test
		this.forma.ajoutMatiere("Gestion des Systemes d'Information", 5);
		Map<String, Integer> mat = this.forma.getCollMatieres();
		//assertion
		assertEquals("La matiere Gestion des Systemes d'Information devrait etre ajoutee a la liste des matieres", (int)5, (int)mat.get("Gestion des Systemes d'Information"));
	}

	/**
	 * Methode testAjoutMatiere2 qui permet de tester l'ajout d'une matiere deja existante dans une formation
	 * 
	 * @throws FormaException
	 */
	@Test(expected = FormaException.class)
	public void testAjoutMatiere2() throws FormaException {
		//instructions de test
		this.forma.ajoutMatiere("Maths", 8);
		Map<String, Integer> mat = this.forma.getCollMatieres();
		//assertion
		assertEquals("La matiere Maths devrait etre ajoutee a la liste des matieres", (int)8, (int)mat.get("Maths"));
	}
	
	/**
	 * Methode testSupprMatiere1 qui permet de tester la suppression d'une matiere d'une formation
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testSupprMatiere1() throws FormaException2 {
		//jeu de donnees
		boolean res;
		//instructions de test
		this.forma.supprMatiere("Maths");
		res = this.forma.getCollMatieres().containsKey(forma);
		//assertion
		assertTrue("La matiere Maths devrait etre supprimee de la liste des matieres", !res);
	}
	
	/**
	 * Methode testSupprMatiere2 qui permet de tester l'ajout et la suppression d'une matiere qui n'etait pas de base dans la formation
	 * 
	 * @throws FormaException
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testSupprMatiere2() throws FormaException, FormaException2 {
		//jeu de donnees
		boolean res;
		//instructions de test
		this.forma.ajoutMatiere("Gestion des Systemes d'Information", 5);
		Map<String, Integer> mat = this.forma.getCollMatieres();
		//assertion
		assertEquals("La matiere Gestion des Systemes d'Information devrait etre ajoutee a la liste des matieres", (int)5, (int)mat.get("Gestion des Systemes d'Information"));
		//instructions de test
		this.forma.supprMatiere("Gestion des Systemes d'Information");
		res = this.forma.getCollMatieres().containsKey(forma);
		//assertion
		assertTrue("La matiere Gestion des Systemes d'Information devrait etre supprimee de la liste des matieres", !res);
	}
	
	/**
	 * Methode testSupprMatiere3 qui permet de tester la suppression d'une matiere qui n'est pas dans la formation
	 * 
	 * @throws FormaException2
	 */
	@Test(expected = FormaException2.class)
	public void testSupprMatiere3() throws FormaException2 {
		//jeu de donnees
		boolean res;
		//instructions de test
		this.forma.supprMatiere("Gestion des Systemes d'Information");
		res = this.forma.getCollMatieres().containsKey(forma);
		//assertion
		assertTrue("La matiere Gestion des Systemes d'Information devrait etre supprimee de la liste des matieres", !res);
	}
	
	/**
	 * Methode testCoeffMat1 qui permet de tester le coefficient d'une matiere dans une formation
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCoeffMat1() throws FormaException2 {
		//instruction de test
		this.forma.coeffMat("Anglais");
		//assertion
		assertEquals("Le coefficient de la matiere Anglais devrait etre de 6.", (Integer)6, this.forma.getCollMatieres().get("Anglais"));
	}
	
	/**
	 * Methode testCoeffMat qui permet de tester l'ajout et le coefficient d'une matiere dans une formation
	 * 
	 * @throws FormaException
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCoeffMat2() throws FormaException, FormaException2 {
		//instructions de test
		this.forma.ajoutMatiere("Gestion des Systemes d'Information", 5);
		Map<String, Integer> mat = this.forma.getCollMatieres();
		//assertion
		assertEquals("La matiere Gestion des Systemes d'Information devrait etre ajoutee a la liste des matieres", (int)5, (int)mat.get("Gestion des Systemes d'Information"));
		//instruction de test
		this.forma.coeffMat("Gestion des Systemes d'Information");
		//assertion
		assertEquals("Le coefficient de la matiere Gestion des Systemes d'Information devrait etre de 5.", (Integer)5, this.forma.getCollMatieres().get("Gestion des Systemes d'Information"));
	}
	
	/**
	 * Methode testCoeffMat qui permet de tester le coefficient d'une matiere qui n'est pas dans la formation
	 * 
	 * @throws FormaException2
	 */
	@Test(expected = FormaException2.class)
	public void testCoeffMat3() throws FormaException2 {
		//instruction de test
		this.forma.coeffMat("Fontionnement des Organisations");
		//assertion
		assertEquals("Le coefficient de la matiere Fontionnement des Organisations devrait etre de 3.", (Integer)3, this.forma.getCollMatieres().get("Fontionnement des Organisations"));
	}
	
}