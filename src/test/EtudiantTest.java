package test;

import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;
import classe.*;
import exception.FormaException2;

/**
 * Classe de tests de la classe Etudiant
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class EtudiantTest {

	/**
	 * Identite de l'etudiant
	 */
	private Identite ident;
	/**
	 * Formation que sui l'etudiant
	 */
	private Formation forma;
	/**
	 * Etudiant
	 */
	private Etudiant etu;
	
	/**
	 * Methode creerEtu qui permet d'initialiser les tests en creant un etudiant
	 */
	@Before
	public void creerEtu() {
		this.ident = new Identite("123456","Diebold","Aymerik");
		Map<String, Integer> mat = new HashMap<String, Integer>();
		mat.put("Maths", 8);
		mat.put("Informatique", 12);
		mat.put("Anglais", 6);
		mat.put("Conception et Programmation Objet Avancees", 10);
		mat.put("Services Reseau", 4);
		this.forma = new Formation(789, mat);
		this.etu = new Etudiant(ident, forma);
	}
	
	/**
	 * Methode testAjoutNote1 qui permet de tester l'ajout d'une note a un etudiant
	 */
	@Test
	public void testAjoutNote1() {
		//instructions de test
		this.etu.ajoutNote("Informatique", 15.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Informatique");
		//assertion
		assertEquals("La note ajoutee en Informatique devrait etre de 15,00.", (Double)15.00, listeNote.get(0));
	}
	
	/**
	 * Methode testAjoutNote2 qui permet de tester l'ajout d'une note nulle a un etudiant
	 */
	@Test
	public void testAjoutNote2() {
		//instructions de test
		this.etu.ajoutNote("Anglais", 0.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Anglais");
		//assertion
		assertEquals("La note ajoutee en Anglais devrait etre de 0,00.", (Double)0.00, listeNote.get(0));
	}

	/**
	 * Methode testAjoutNote3 qui permet de tester l'ajout d'une note negative a un etudiant
	 */
	@Test
	public void testAjoutNote3() {
		//instructions de test
		this.etu.ajoutNote("Conception et Programmation Objet Avancees", -10.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Conception et Programmation Objet Avancees");
		//assertion
		assertEquals("La note qui veut etre ajoutee en CPOA ne devrait pas etre ajoutee.", 0, listeNote.size());
	}
	
	/**
	 * Methode testAjoutNote4 qui permet de tester l'ajout d'une note a un etudiant pour une matiere qui n'existe pas
	 */
	@Test
	public void testAjoutNote4() {
		//instruction de test
		this.etu.ajoutNote("Gestion des Systemes d'Information", 10.00);
		//assertion
		assertEquals("La note que l'on veut ajouter en Gestion des Systemes d'Information ne devrait pas etre ajoutee.", null, this.etu.rechercheNote("Gestion des Systemes d'Information"));
	}
	
	/**
	 * Methode testSupprNote1 qui permet de tester la suppression d'une note d'un etudiant
	 */
	@Test
	public void testAjoutNote5() {
		//
		this.etu.ajoutNote("Conception et Programmation Objet Avancees", 155.00);
		//
		ArrayList<Double> listeNote = this.etu.rechercheNote("Conception et Programmation Objet Avancees");
		//
		assertEquals("La note qui veut etre ajoutee en CPOA ne devrait pas etre ajoutee.", 0, listeNote.size());
	}
	
	/**
	 * Methode testSupprNote2 qui permet de tester la suppression d'une note d'un etudiant
	 */
	@Test
	public void testSupprNote1() {
		//instruction de test
		this.etu.ajoutNote("Anglais", 12.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Anglais");
		//assertion
		assertEquals("La note ajoutee en Informatique devrait etre de 12,00.", (Double)12.00, listeNote.get(0));
		//instruction de test
		this.etu.supprNote("Anglais", 12.00);
		//assertion
		assertEquals("La note devrait etre supprimee de la liste des notes d'Anglais.", 0, listeNote.size());
		
	}
	
	/**
	 * Methode testSupprNote2 qui permet de tester la suppression d'une note qui n'est pas une de celles d'un etudiant
	 */
	@Test
	public void testSupprNote2() {
		//instructions de test
		this.etu.ajoutNote("Anglais", 12.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Anglais");
		//assertion
		assertEquals("La note ajoutee en Anglais devrait etre de 12,00.", (Double)12.00, listeNote.get(0));
		//instruction de test
		this.etu.supprNote("Anglais", 15.00);
		//assertions
		assertEquals("La liste devrait contenir une seule valeur, qui est 12,00.", 1, listeNote.size());
		assertEquals("La valeur restante dans la liste devrait etre 12,00.", (Double)12.00, listeNote.get(0));
	}
	
	/**
	 * Methode testSupprNote3 qui permet de tester la suppression d'une note d'un etudiant pour une matiere qui n'existe pas
	 */
	@Test
	public void testSupprNote3() {
		//instructions de test
		this.etu.ajoutNote("Gestion des Systemes d'Information", 12.00);
		this.etu.supprNote("Gestion des Systemes d'Information", 12.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Gestion des Systemes d'Information");
		//assertion
		assertEquals("La liste devrait etre inexistante.", null, listeNote);
	}
	
	/**
	 * Methode testSupprNote4 qui permet de tester la suppression d'une note qui n'existe pas
	 */
	@Test
	public void testSupprNote4() {
		//instructions de test
		this.etu.ajoutNote("Anglais", 12.00);
		this.etu.supprNote("Anglais", 154.00);
		ArrayList<Double> listeNote = this.etu.rechercheNote("Anglais");
		//assertion
		assertEquals("Il devrait rester un element dans la liste, une note de 12,00 en Anglais.", (Double)12.00, listeNote.get(0));
	}
	
	/**
	 * Methode testCamcMoyMat1 qui permet de tester le calcul de la moyenne d'un etudiant pour une matiere
	 */
	@Test
	public void testCalcMoyMat1() {
		//instructions de test
		this.etu.ajoutNote("Maths", 18.00);
		this.etu.ajoutNote("Maths", 13.00);
		this.etu.ajoutNote("Maths", 3.25);
		this.etu.ajoutNote("Maths", 7.50);
		this.etu.ajoutNote("Maths", 1.25);
		this.etu.ajoutNote("Maths", 20.00);
		//assertion
		assertEquals("La moyenne d'Aymerik en Maths devrait etre egale a 10,50.", (Double)10.50, (Double)this.etu.calcMoyMat("Maths"));
	}
	
	/**
	 * Methode testCamcMoyMat2 qui permet de tester le calcul de la moyenne d'un etudiant pour une matiere, pour laquelle l'etudiant n'a aucune note
	 */
	@Test
	public void testCalcMoyMat2() {
		//assertion
		assertEquals("La moyenne d'Aymerik en Anglais devrait etre egale a 0,00.", (Double)0.00, (Double)etu.calcMoyMat("Anglais"));
	}
	
	/**
	 * Methode testCamcMoyMat3 qui permet de tester le calcul de la moyenne d'un etudiant pour une matiere qui n'existe pas
	 */
	@Test
	public void testCalcMoyMat3() {
		//assertion
		assertEquals("La moyenne d'Aymerik en Gestion des Systemes d'Information ne devrait pas exister.", (Double)0.00, (Double)this.etu.calcMoyMat("Gestion des Systemes d'Information"));
	}
	
	/**
	 * Methode testCamcMoyGenerale1 qui permet de tester le calcul de la moyenne generale d'un etudiant
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalcMoyGenerale1() throws FormaException2 {
		//instructions de test
		this.etu.ajoutNote("Maths", 16.00);
		this.etu.ajoutNote("Maths", 14.00);
		this.etu.ajoutNote("Anglais", 15.00);
		this.etu.ajoutNote("Informatique", 15.00);
		this.etu.ajoutNote("Informatique", 10.00);
		this.etu.ajoutNote("Informatique", 20.00);
		//assertion
		assertEquals("La moyenne generale d'Aymerik devrait etre de 15,00.", (Double)15.00, (Double)this.etu.calcMoyGenerale());
	}
	
	/**
	 * Methode testCamcMoyGenerale2 qui permet de tester le calcul de la moyenne generale d'un etudiant qui n'a aucune note
	 * 
	 * @throws FormaException2
	 */
	@Test
	public void testCalcMoyGenerale2() throws FormaException2 {
		//assertion
		assertEquals("La moyenne generale d'Aymerik devrait etre de 0,00.", (Double)0.00, (Double)this.etu.calcMoyGenerale());
	}
	
}