package classe;

import java.util.*;
import exception.FormaException2;

/**
 * Classe Etudiant
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class Etudiant{
	
	/**
	 * Identite d'un etudiant
	 */
	private Identite identite;
	/**
	 * Formation suivie par un etudiant
	 */
	private Formation forma;
	/**
	 * Collection de notes par matiere d'un etudiant
	 */
	private Map<String,ArrayList<Double>> resultats;
	
	/**
	 * Constructeur de la classe Etudiant
	 * 
	 * @param i
	 * 		Identite d'un etudiant
	 * @param f
	 * 		Formation suivie par un etudiant
	 */
	public Etudiant(Identite i,Formation f){
		this.identite = i;
		this.resultats = new HashMap<String,ArrayList<Double>>();
		this.forma = f;
		Map<String,Integer> colMat = forma.getCollMatieres();
		Set<String> cle = colMat.keySet();
		for(String n:cle){
			ArrayList<Double> listeNote = new ArrayList<Double>();
			this.resultats.put(n, listeNote);
		}
	}
	
	/**
	 * Methode ajoutNote qui permet d'ajouter une matiere et une note dans la liste des resultats de l'etudiant
	 * 
	 * @param mat
	 * 		Matiere que l'on souhaite ajouter dans la collection de notes par matiere d'un etudiant
	 * @param note
	 * 		Note que l'on souhaite ajouter dans la collection de notes par matiere d'un etudiant
	 */
	public void ajoutNote(String mat, double note){
		if(this.resultats.containsKey(mat) && note>=0 && note<=20){	
			this.resultats.get(mat).add(note);
		}
	}
	
	/**
	 * Methode supprNote qui permet de supprimer une matiere et une note dans la liste des resultats de l'etudiant
	 * 
	 * @param mat
	 * 		Matiere que l'on souhaite supprimer dans la collection de notes par matiere d'un etudiant
	 * @param note
	 * 		Note que l'on souhaite supprimer dans la collection de notes par matiere d'un etudiant
	 */
	public void supprNote(String mat,double note){
		if(this.resultats.containsKey(mat)){
			this.resultats.get(mat).remove(note);
		}
	}
	
	/**
	 * Methode calcMoyMat qui retourne la moyenne d'un etudiant pour une matiere passee en parametre
	 * 
	 * @param mat
	 * 		Matiere pour laquelle on veut avoir la moyenne d'un etudiant
	 * 
	 * @return la moyenne d'un etudiant pour une matiere donnee
	 */
	public double calcMoyMat(String mat){
		double res = 0.00,somme = 0.0;
		int i=0;
		if (this.resultats.containsKey(mat)){
			List<Double> l = this.resultats.get(mat);
			for(Double n:l){
				somme += n;
				i++;
			}
			if (i == 0){
				i = 1;
			}
			res = somme / i;
		}
		return res;
	}
	
	/**
	 * Methode calcMoyGenerale qui retourne la moyenne generale d'un etudiant
	 * 
	 * @return la moyenne generale d'un etudiant
	 * 
	 * @throws FormaException2
	 */
	public double calcMoyGenerale() throws FormaException2 {
		Set<String> cle = this.resultats.keySet();
		double somme = 0;
		double coeff= 0;
		double sommeCoeff = 0;
		for(String n : cle){
			ArrayList<Double> liste_note = this.resultats.get(n);
			coeff = this.forma.coeffMat(n);
			if(liste_note.size() != 0){
				somme = somme + this.calcMoyMat(n) * coeff;
				sommeCoeff = sommeCoeff + coeff;
			}
		}
		if (sommeCoeff == 0){
			sommeCoeff = 1;
		}
		return somme/sommeCoeff;
	}
	
	/**
	 * Methode rechercheNote qui retourne la liste des notes associees a une matiere donnee pour un etudiant
	 * 
	 * @param mat
	 * 		Matiere pour laquelle on veut avoir les notes d'un etudiant
	 * 
	 * @return la liste des notes associees a une matiere donnee pour un etudiant
	 */
	public ArrayList<Double> rechercheNote(String mat){
		return this.resultats.get(mat);
	}
	
	/**
	 * Methode getNomEtu qui retourne le nom de l'etudiant
	 * 
	 * @return le nom de l'etudiant
	 */
	public String getNomEtu(){
		return this.identite.getNom();
	}
	
	/**
	 * Methode getForma qui retourne la formation de l'etudiant
	 * 
	 * @return la formation de l'etudiant
	 */
	public Formation getForma(){
		return this.forma;
	}
	
}