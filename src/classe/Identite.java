package classe;

/**
 * Classe Identite
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class Identite {

	/**
	 * Numero d'identification personnel d'un etudiant
	 */
	private String nip;
	/**
	 * Nom d'un etudiant
	 */
	private String nom;
	/**
	 * Prenom d'un etudiant
	 */
	private String prenom;
	
	/**
	 * Constructeur de la classe Identite
	 * 
	 * @param nouvNip
	 * 		Numero d'identification personnel d'un etudiant
	 * @param n
	 * 		Nom d'un etudiant
	 * @param p
	 * 		Prenom d'un etudiant
	 */
	public Identite(String nouvNip, String n, String p) {
		this.nip = nouvNip;
		this.nom = n;
		this.prenom = p;
	}
	
	/**
	 * Methode getNip qui retourne le numero d'identification personnel d'un etudiant
	 * 
	 * @return le numero d'identification personnel d'un etudiant
	 */
	public String getNip() {
		return this.nip;
	}
	
	/**
	 * Methode getNom qui retoune le nom d'un etudiant
	 * 
	 * @return le nom d'un etudiant
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Methode getPrenom qui retourne le prenom d'un etudiant
	 * 
	 * @return le prenom d'un etudiant
	 */
	public String getPrenom() {
		return this.prenom;
	}
	
	/**
	 * Methode setNip qui permet de modifier le numero d'identification personnel d'un etudiant
	 * 
	 * @param nouveauNip
	 * 		Nouveau numero d'identification personnel d'un etudiant
	 */
	public void setNip(String nouveauNip) {
		this.nip = nouveauNip;
	}
	
	/**
	 * Methode setNom qui permet de modifier le nom d'un etudiant
	 * 
	 * @param name
	 * 		Nouveau nom d'un etudiant
	 */
	public void setNom(String name) {
		this.nom = name;
	}
	
	/**
	 * Methode setPrenom qui permet de modifier le prenom d'un etudiant
	 * 
	 * @param vorname
	 * 		Nouveau prenom de l'etudiant
	 */
	public void setPrenom(String vorname) {
		this.prenom = vorname;
	}
	
}