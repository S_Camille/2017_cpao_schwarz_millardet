package classe;

import java.util.*;
import exception.*;

/**
 * Classe Formation
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class Formation {

	/**
	 * Identifiant d'une formation
	 */
	private int id;
	/**
	 * Collection des matieres qui sont enseignees dans une formation avec leurs coefficients
	 */
	private Map<String,Integer> collMatieres;

	/**
	 * Constructeur de la classe Formation
	 * 
	 * @param nouvId
	 * 		Identifiant d'une formation
	 * @param matieres
	 * 		Collection des matieres qui sont enseignees dans une formation avec leurs coefficients
	 */
	public Formation(int nouvId, Map<String,Integer> matieres) {
		this.id = nouvId;
		this.collMatieres = matieres;
	}
	
	/**
	 * Methode getId qui retourne l'identifiant d'une formation
	 * 
	 * @return l'identifiant d'une formation
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Methode setId qui permet de modifier l'identifiant d'une formation
	 * 
	 * @param newId
	 * 		Nouveau identifiant d'une formation
	 */
	public void setId(int newId) {
		this.id = newId;
	}
	
	/**
	 * Methode getCollMatieres qui retourne la collection des matieres qui sont enseignees dans une formation avec leurs coefficients
	 * 
	 * @return la collection des matieres qui sont enseignees dans une formation avec leurs coefficients
	 */
	public Map<String,Integer> getCollMatieres() {
		return this.collMatieres;
	}
	
	/**
	 * Methode ajoutMatiere qui permet d'ajouter une matiere et un coefficient dans la collection de matieres
	 * 
	 * @param mat
	 * 		Matiere que l'on souhaite ajouter dans la collection de matieres
	 * @param coeff
	 * 		Coefficient de la matiere mat, que l'on souhaite ajouter dans la collection de matieres
	 * 
	 * @throws FormaException
	 */
	public void ajoutMatiere(String mat, int coeff) throws FormaException {
		if (this.collMatieres.containsKey(mat)) {
			throw new FormaException(mat);
		}
		else {
			this.collMatieres.put(mat, coeff);
		}
	}
	
	/**
	 * Methode supprMatiere qui permet de supprimer une matiere dans
	 * 
	 * @param mat
	 * 		Matiere que l'on souhaite supprimer de la collection de matieres
	 * 
	 * @throws FormaException2
	 */
	public void supprMatiere(String mat) throws FormaException2 {
		if (!this.collMatieres.containsKey(mat)) {
			throw new FormaException2(mat);
		}
		else {
			this.collMatieres.remove(mat);
		}
	}
	
	/**
	 * Methode coeffMat qui retourne le coefficient d'une matiere passee en parametre
	 * 
	 * @param mat
	 * 		Matiere dont l'on souhaite connaitre le coefficient	
	 * 
	 * @return le coefficient d'une matiere passee en parametre
	 * 
	 * @throws FormaException2
	 */
	public int coeffMat(String mat) throws FormaException2 {
		if (!this.collMatieres.containsKey(mat)) {
			throw new FormaException2(mat);
		}
		else {
			return this.collMatieres.get(mat);
		}
	}
	
}