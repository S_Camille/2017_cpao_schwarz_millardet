package classe;

import java.util.*;
import comparator.*;
import exception.FormaException2;

/**
 * Classe Groupe
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class Groupe {
	
	 /**
	  * Collection des etudiants qui composent le groupe
	  */
	 private List<Etudiant> collEtudiants;
	 /**
	  * Formation a laquelle le groupe d'etudiants appartient
	  */
	 private Formation formation;
	 
	 /**
	  * Constructeur de la classe Groupe
	  * 
	  * @param f
	  * 	 Formation a laquelle le groupe d'etudiants appartient
	  */
	 public Groupe(Formation f){
		 this.collEtudiants = new ArrayList<Etudiant>();
		 this.formation = f;
	 }
	 
	 /**
	  * Methode ajoutEtu qui permet d'ajouter un etudiant dans la collection des etudiants qui composent le groupe
	  * 
	  * @param etu
	  * 	 Etudiant que l'on souhaite ajouter dans le groupe
	  */
	 public void ajoutEtu(Etudiant etu){
		 if(!this.collEtudiants.contains(etu) && etu.getForma() == this.formation){
			 this.collEtudiants.add(etu);
		 }
	 }
	 
	 /**
	  * Methode ajoutEtu qui permet de supprimer un etudiant dans la collection des etudiants qui composent le groupe
	  * 
	  * @param etu
	  * 	 Etudiant que l'on souhaite supprimer dans le groupe
	  */
	 public void supprEtu(Etudiant etu){
		 if(this.collEtudiants.contains(etu)){
			 this.collEtudiants.remove(etu);
		 }
	 }
	 
	 /**
	  * Methode calcMoyMatGr qui retourne la moyenne d'un groupe d'etudiants dans une matiere donnee
	  * 
	  * @param mat
	  * 	 Matiere pour laquelle on veut avoir la moyenne d'un groupe d'etudiants
	  * 
	  * @return la moyenne d'un groupe d'etudiants dans une matiere donnee
	  */
	 public double calcMoyMatGr(String mat){
		 double somme = 0.00;
		 Double note = 0.00;
		 int nbEtudiant = 0;
		 for(Etudiant n:this.collEtudiants){
			 if(n.rechercheNote(mat) != null){
				 if(n.rechercheNote(mat).size() != 0){
					 note = n.calcMoyMat(mat);
					 if (note != null){
						 somme = somme + note;
						 nbEtudiant++;
					 }
				 }
			 }
		 }
		 if (nbEtudiant == 0){
			 nbEtudiant = 1;
		 }
		 return somme/nbEtudiant;
	 }
	 
	 /**
	  * Methode calcMoyGeneraleGr qui retourne la moyenne generale d'un groupe d'etudiants
	  * 
	  * @return la moyenne generale d'un groupe d'etudiants
	  * 
	  * @throws FormaException2
	  */
	 public double calcMoyGeneraleGr() throws FormaException2 {
		 double moyenne = 0.00;
		 int nb = 0;
		 for(Etudiant n:this.collEtudiants){
			 moyenne = moyenne + n.calcMoyGenerale();
			 nb++;
		 }
		 if (nb == 0){
			 nb = 1;
		 }
		 return moyenne/nb;
	 }
	 
	 /**
	  * Methode getFormation qui retourne la formation a laquelle le groupe d'etudiants appartient
	  * 
	  * @return la formation a laquelle le groupe d'etudiants appartient
	  */
	 public Formation getFormation() {
		 return this.formation;
	 }
	 
	 /**
	  * Methode getCollEtudiants qui retourne la collection des etudiants qui composent le groupe
	  * 
	  * @return la collection des etudiants qui composent le groupe
	  */
	 public List<Etudiant> getCollEtudiants(){
		 return this.collEtudiants;
	 }
	 
	 /**
	  * Methode triParMerite qui permet de trier la collection des etudiants qui composent le groupe, selon leur moyenne generale decroissante
	  */
	 public void triParMerite(){
		 Collections.sort(this.collEtudiants, new GroupeMeriteComprator());
		 System.out.println("Dans tri par merite : ");
	 }
	 
	 /**
	  * Methode triAlpha qui permet de trier la collection des etudiants qui composent le groupe, selon leur ordre alphabetique croissant
	  */
	 public void triAlpha(){
		 Collections.sort(this.collEtudiants, new GroupeAlphaComparator());
		 System.out.println("Dans tri alphabetique : ");
	 }
	 
}