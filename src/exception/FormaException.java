package exception;

@SuppressWarnings("serial")

/**
 * Classe FormaException qui herite de la classe Exception
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class FormaException extends Exception {

	/**
	 * Matiere causant la creation de l'exception
	 */
	private String mat1;
	
	/**
	 * Constructeur de la classe FormaException
	 * 
	 * @param m1
	 * 		Matiere causant la creation de l'exception
	 */
	public FormaException(String m1) {
		this.mat1 = m1;
	}
	
	/**
	 * Methode toString qui retourne "Meme matiere : " + this.mat1 + " = " + this.mat1
	 * 
	 * @return "Meme matiere : " + this.mat1 + " = " + this.mat1
	 */
	public String toString() {
		return "Meme matiere : " + this.mat1 + " = " + this.mat1;
	}
	
}