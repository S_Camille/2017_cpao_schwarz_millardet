package exception;

@SuppressWarnings("serial")

/**
 * Classe FormaException2 qui herite de la classe Exception
 * 
 * @author S_Camille
 * @author millarde1u
 */
public class FormaException2 extends Exception {

	/**
	 * Matiere causant la creation de l'exception
	 */
	private String mat1;
	
	/**
	 * Constructeur de la classe FormaException2
	 * 
	 * @param m1
	 * 		Matiere causant la creation de l'exception
	 */
	public FormaException2(String m1) {
		this.mat1 = m1;
	}
	
	/**
	 * Methode toString qui retourne "La matiere " + this.mat1 + " n'existe pas dans la liste des mati�res"
	 * 
	 * @return "La matiere " + this.mat1 + " n'existe pas dans la liste des matieres"
	 */
	public String toString() {
		return "La matiere " + this.mat1 + " n'existe pas dans la liste des matieres";
	}
	
}